from flask import Flask

app = Flask(__name__)


@app.route("/")
def index():
    return "Hello Patrick"


@app.rout("/second_page")
def second_page():
    return "Hello Arie"


if __name__ == "__main__":
    app.run()
