# Python project


## Getting started

### Install Python

Visit: https://www.python.org/downloads/

### Install virtualenv

`pip install virualenv`

### Create the virtual environment

`virtualenv venv --python=python3`


### Activate the virtual environment

For UNIX:

* `source venv/bin/activate`

For Windows:

* `venv\Scripts\activate`


### Activate the virtual environment

`pip install -r requirements.txt`


### Run the app


`python app.py`
