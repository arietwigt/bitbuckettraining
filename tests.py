from utils.calc_functions import calc_sum, calc_interest


# tests
def test_calc_sum():
    assert calc_sum(4, 4) == 8
    assert calc_sum(10, 10) == 20


def test_calc_interest():
    assert calc_interest(1000, 0.1, 1) == 1100
