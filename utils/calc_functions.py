from typing import Union


def calc_sum(number_1: Union[int, float],
             number_2: Union[int, float]) -> Union[int, float]:
    '''
    Method for adding two numbers:

    * number_1: First number
    * number_2: Second number
    '''

    # add the two numbers
    result = number_1 + number_2

    return result


def calc_interest(amount: Union[int, float],
                  interest_rate: Union[int, float],
                  years: int) -> Union[float, int]:
    '''
    Method for calculating the amount after interest for Method for calculating the amount after interest for
    ceratain amount of years

    * amount: The amount
    * interest_rate: Interest rate in fractions
    * years: Whole years
    '''
    # calculate the amount
    result = amount * ((1 + interest_rate) ** years)

    return result
